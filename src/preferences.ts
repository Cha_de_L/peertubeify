/* This file is part of PeerTubeify.
 *
 * PeerTubeify is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * PeerTubeify is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * PeerTubeify. If not, see <https://www.gnu.org/licenses/>.
 */

import * as _ from 'lodash/fp';
import * as browser from 'webextension-polyfill';
import constants from './constants';
import { RedirectType } from './types';

const stripProtocol = _.replace(/^https?:\/\//, '');

export default class Preferences {
    private _searchInstance: string;
    openInOriginalInstance: boolean;
    resumeUncompletedVideo: boolean;
    redirectYoutube: RedirectType;
    redirectPeertube: RedirectType;

    constructor(localStorage) {
        this.searchInstance = _.defaultTo(constants.peertubeAPI.defaultInstance, localStorage.searchInstance as string);
        this.openInOriginalInstance = _.defaultTo(true, localStorage.openInOriginalInstance as boolean);
        this.resumeUncompletedVideo = _.defaultTo(true, localStorage.resumeUncompletedVideo as boolean);
        this.redirectYoutube = _.defaultTo(RedirectType.Show, localStorage.redirectYoutube);
        this.redirectPeertube = _.defaultTo(RedirectType.None, localStorage.redirectPeertube);
    }

    static async getPreferences() {
        const localStorage = await browser.storage.local.get();
        return new Preferences(localStorage);
    }

    async save() {
        await browser.storage.local.set({
            searchInstance: this.searchInstance,
            openInOriginalInstance: this.openInOriginalInstance,
            resumeUncompletedVideo: this.resumeUncompletedVideo,
            redirectYoutube: this.redirectYoutube,
            redirectPeertube: this.redirectPeertube,
        })
        const prefs = await browser.storage.local.get()
    }

    get searchInstance() {
        return this._searchInstance;
    }

    set searchInstance(instance) {
        this._searchInstance = _.isEmpty(instance) ? constants.peertubeAPI.defaultInstance : stripProtocol(instance)
    }
}
